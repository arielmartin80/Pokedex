<?php

session_start();
if(isset($_SESSION['user'])!='ok'){
  header("Location: recursos/img/index.html");
  die();
}
  include('header.php');
?>

<main>

  <div class="form">
    
    <form action="controller/controller_agregar.php" method="post">
        
        <h2>Nombre</h2>
        <input type="text" id="nombre" placeholder="Ingrese nombre del Pokemon" name="nombre" required><br><br>
        
        <h2>Link de la imagen</h2>
        <input type="text" id="imagen" placeholder=" Ingrese url" name="imagen" required><br><br>

        <h2>Ataque</h2>
        <input type="text" id="ataque" placeholder="Ingrese nombre del ataque" name="ataque" required><br><br>

        <h2>Tipo</h2>
        <select id="tipo" name="tipo">
          <option value="recursos/img/electric.jpg">Electrico</option>
          <option value="recursos/img/fire.jpg">Fuego</option>
          <option Value="recursos/img/grass.jpg">Planta</option>
          <option Value="recursos/img/agua.jpg">Agua</option>
          <option Value="recursos/img/bicho.jpg">Bicho</option>
        </select>
        <br><br>

        <button type="submit" class='w3-button w3-blue w3-round'>Guardar</button>
  
    </form>

  </div>

</main>

 <?php include('footer.php'); ?>

</body>

</html>