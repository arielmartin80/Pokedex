<?php

session_start();
if(isset($_SESSION['user'])!='ok'){
  header("Location: recursos/img/index.html");
  die();
}

  $nombre=$_GET['nombre'];
  $imagen=$_GET['imagen'];
  $ataque=$_GET['ataque'];
  $id=$_GET['id'];
  //$tipo=$_GET['tipo']; 

  include('header.php');
?>

<main>

  <div class="form">
    <form action="controller/controller_modificar.php" method="post">
        
        <input id="id" name="id" type="hidden" value="<?php echo $id ?>">

        <h2>Nombre</h2>
        <input type="text" id="nombre" placeholder="Ingrese nombre del Pokemon" name="nombre" required
        value='<?php echo $nombre ?>'/>
        <br>
        
        <h2>Link de la imagen</h2>
        <input type="text" id="imagen" placeholder="Ingrese url" name="imagen" required
        value='<?php echo $imagen ?>'/>
        <br>

        <h2>Ataque</h2>
        <input type="text" id="ataque" placeholder="Ingrese nombre del ataque" name="ataque" required
        value='<?php echo $ataque ?>'/>
        <br>

        <h2>Seleccione un Tipo</h2>
        <select id="tipo" name="tipo">
          <option value="recursos/img/electric.jpg">Electrico</option>
          <option value="recursos/img/fire.jpg">Fuego</option>
          <option Value="recursos/img/grass.jpg">Planta</option>
          <option Value="recursos/img/agua.jpg">Agua</option>
          <option Value="recursos/img/bicho.jpg">Bicho</option>
        </select>
        <br><br>

        <button type="submit" class='w3-button w3-blue w3-round'>Modificar</button>
  
    </form>

  </div>

</main>

 <?php include('footer.php'); ?>

</body>

</html>