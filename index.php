<?php session_start();
include('header.php');
?>

<div class="clear"></div>

<main>

  <div class="form">
    <form action="#" method="get">
        
        <input type="text" id="whoisthatpokemon" placeholder="Ingrese nombre" name="whoisthatpokemon">
        <button type="submit" class='w3-button w3-blue w3-round'>Buscar</button>
        <br><br>

    </form>

    <?php
    if(isset($_SESSION['user'])=='ok'){
      echo"
        <form action='agregar.php' method='get'>
            
            <button type='submit' class='w3-button w3-blue w3-round'>Agregar</button>
            <br><br>

        </form>";
    }
    ?>

  </div>

<table id="customers">
  <tr>
    <th>Imagen</th>
    <th>Nombre</th>
    <th>Ataque</th>
    <th>Tipo</th>
  </tr>

<?php

include './controller/daoPokemon.php';
$dao=new daoPokemon();
$arrayDePokemones = $dao->getAllPokemons() ;

//  si no existe la variable (para el primer ingreso al main)
   if(!isset($_GET["whoisthatpokemon"]) ){
   
//      se muestra el array completo
        foreach ($arrayDePokemones as $nombre => $pokemon) {

        $img_poke = $arrayDePokemones[$nombre]['imagen'];
        $img_tipo = $arrayDePokemones[$nombre]['tipo'];
        $ataque = $arrayDePokemones[$nombre]['ataque'];

        echo"
          <tr>
            <td><img class='img' src='$img_poke' alt='$nombre'></td>
            <td>$nombre</td>
            <td>$ataque</td>
            <td><img class='img' src='$img_tipo' alt=''></td>
          </tr>
          ";}
      }

//    si existe la variable (se hizo una busqueda)
      if(isset($_GET["whoisthatpokemon"]) ){
      
//    si la variable no esta dentro del array (mensaje de no encontrado)
      if(!array_key_exists($_GET["whoisthatpokemon"],$arrayDePokemones)){
 
          echo "<div class='mje'>Pokemon no encontrado</div><br>";
      }

//    si la variable esta dentro del array (solo se muestra un resultado)
      if(array_key_exists($_GET["whoisthatpokemon"],$arrayDePokemones)){
    
        $nombre = $_GET["whoisthatpokemon"];
        $img_poke = $arrayDePokemones[$nombre]['imagen'];
        $img_tipo = $arrayDePokemones[$nombre]['tipo'];
        $ataque = $arrayDePokemones[$nombre]['ataque'];

        $id=$dao->getIdByNombre($nombre);

          echo"
          <tr>
            <td><img class='img' src='$img_poke' alt='$nombre'></td>
            <td>$nombre</td>
            <td>$ataque</td>
            <td><img class='img' src='$img_tipo' alt=''></td>
          </tr>
          "; 

          echo "</table>"; 
        }

          echo "<br>";

    if(array_key_exists($_GET["whoisthatpokemon"],$arrayDePokemones) && isset($_SESSION['user'])=='ok'){
   
          echo "<div class='center'>";
          echo "<a href='modificar.php?nombre=$nombre&imagen=$img_poke&ataque=$ataque&id=$id'>
                     <input type='button' value='Modificar' class='w3-button w3-blue w3-round'>
                </a>";
          echo "<a href='controller/controller_eliminar.php?id=$id'>
                      <input type='button' value='Eliminar' class='w3-button w3-blue w3-round'>
                </a>";
          echo "</div>";
       } 



//     si la variable no esta dentro del array (muestra el array completo)
       if (!array_key_exists($_GET["whoisthatpokemon"],$arrayDePokemones) ){
 
        foreach ($arrayDePokemones as $nombre => $pokemon) {

        $img_poke = $arrayDePokemones[$nombre]['imagen'];
        $img_tipo = $arrayDePokemones[$nombre]['tipo'];
        $ataque = $arrayDePokemones[$nombre]['ataque'];

        echo"
          <tr>
            <td><img class='img' src='$img_poke' alt='$nombre'></td>
            <td>$nombre</td>
            <td>$ataque</td>
            <td><img class='img' src='$img_tipo' alt=''></td>
          </tr>
          ";}
        }
  }   

?>

</table>

</main>

<?php include('footer.php'); ?>

</body>

</html>