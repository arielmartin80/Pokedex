<?php
	include 'conexion.php';

	class daoPokemon{


	public function getIdByNombre($nombre){

		$db = new BaseDatos();
		$conn=$db->conectar();

		$SQL= "select id from pokemones where nombre='$nombre'";	

		//echo $SQL;

		$result = mysqli_query($conn, $SQL);
		
		if ($result){

			$array = mysqli_fetch_assoc($result) ;
			$pokemonId = $array['id'];
		}

		$db->desconectar();
		
		return $pokemonId;

	}


	public function getAllPokemons(){

		$db = new BaseDatos();
		$conn=$db->conectar();

		$sql="select * from pokemones";

		$result=mysqli_query($conn, $sql);

		$arrayDePokemones = array();

		while($rows=mysqli_fetch_assoc($result) ){

		$arrayDePokemones[$rows['nombre']] = ["tipo"=>$rows['tipo'],"imagen"=>$rows['imagen'],"ataque"=>$rows['ataque'] ];

		}

		$db->desconectar();

		return $arrayDePokemones;
	}


	public function savePokemon($nombre, $imagen, $ataque, $tipo){

		$db = new BaseDatos();
		$conn=$db->conectar();	

		$SQL = 	"INSERT INTO pokemones (nombre, imagen, ataque, tipo)
				 VALUES ('$nombre', '$imagen', '$ataque', '$tipo')";

		//echo $SQL;

		mysqli_query($conn, $SQL);
		
		$db->desconectar();

	}

	public function updatePokemon($nombre, $imagen, $ataque, $tipo, $id){

		$db = new BaseDatos();
		$conn=$db->conectar();	

		$SQL = 	"UPDATE pokemones
				 SET nombre = '$nombre', imagen = '$imagen', ataque = '$ataque', tipo = '$tipo'
				 WHERE id = '$id' ";

		//echo $SQL;

		mysqli_query($conn, $SQL);
		
		$db->desconectar();

	}

	public function deletePokemon($id){

		$db = new BaseDatos();
		$conn=$db->conectar();	

		$SQL = 	"DELETE FROM pokemones WHERE id = '$id' ";

		//echo $SQL;

		$result=mysqli_query($conn, $SQL);
		
		$db->desconectar();

	}


}

?>