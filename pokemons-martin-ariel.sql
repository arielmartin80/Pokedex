-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para pokemons-martin-ariel
CREATE DATABASE IF NOT EXISTS `pokemons-martin-ariel` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `pokemons-martin-ariel`;

-- Volcando estructura para tabla pokemons-martin-ariel.pokemones
CREATE TABLE IF NOT EXISTS `pokemones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ataque` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla pokemons-martin-ariel.pokemones: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `pokemones` DISABLE KEYS */;
INSERT IGNORE INTO `pokemones` (`id`, `imagen`, `nombre`, `ataque`, `tipo`) VALUES
	(2, 'https://vignette.wikia.nocookie.net/pokemon/images/0/0d/025Pikachu.png', 'Pikachu', 'Electric-volt', 'recursos/img/electric.jpg'),
	(4, 'https://vignette.wikia.nocookie.net/pokemon/images/7/73/004Charmander.png', 'Charmander', 'llamarada', 'recursos/img/fire.jpg'),
	(6, 'https://vignette.wikia.nocookie.net/pokemon/images/2/21/001Bulbasaur.png', 'Bulbasaur', 'overgrow', 'recursos/img/grass.jpg'),
	(7, 'https://vignette.wikia.nocookie.net/es.pokemon/images/b/be/Venusaur.png', 'Venusaur', 'Espesura', 'recursos/img/grass.jpg'),
	(8, 'https://vignette.wikia.nocookie.net/es.pokemon/images/0/05/Caterpie.png', 'Caterpie', 'Polvo escudo', 'recursos/img/bicho.jpg'),
	(9, 'https://vignette.wikia.nocookie.net/es.pokemon/images/e/e3/Squirtle.png', 'Squirtle', 'Torrente', 'recursos/img/agua.jpg');
/*!40000 ALTER TABLE `pokemones` ENABLE KEYS */;

-- Volcando estructura para tabla pokemons-martin-ariel.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla pokemons-martin-ariel.usuarios: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT IGNORE INTO `usuarios` (`id`, `usuario`, `password`) VALUES
	(1, 'ariel', '81dc9bdb52d04dc20036dbd8313ed055');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
