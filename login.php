<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" type="text/css" href="recursos/css/util.css">
	<link rel="stylesheet" type="text/css" href="recursos/css/main.css">
	<link rel="stylesheet" type="text/css" href="recursos/css/estilos.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
				<form class="login100-form validate-form" action="controller/controller_login.php" method="POST" enctype="application/x-www-form-urlencoded">
					<span class="login100-form-title p-b-33">
						Login Pokedex
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="nombre" placeholder="Username">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="wrap-input100 rs1 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="clave" placeholder="Password">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="container-login100-form-btn m-t-20">
						<button class="login100-form-btn">
							Sign in
						</button>
					</div>

					<div class="mje">
						<br><?php if (isset($_GET['mje']) ){ echo $_GET['mje']; }; ?>
					</div>

					<div class="text-center p-t-45 p-b-4">
						<span class="txt1">
							Username: ariel / Password: 1234
						</span>

						<a href="#" class="txt2 hov1">
							
						</a>
					</div>

					<div class="text-center">
						<span class="txt1">
							
						</span>

						<a href="#" class="txt2 hov1">
							
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>